﻿1
00:00:01,479 --> 00:00:03,307
Dans cette courte vidéo, nous allons

2
00:00:03,307 --> 00:00:05,136
mettre en avant le potentiel de Signal,

3
00:00:05,136 --> 00:00:07,577
un outil conçu pour les journalistes afin qu’ils

4
00:00:07,577 --> 00:00:10,019
puissent trouver du contenu Facebook et Instagram

5
00:00:10,019 --> 00:00:12,199
par catégorie de contenu (actualités,

6
00:00:12,199 --> 00:00:14,379
divertissement, sport, etc.)

7
00:00:14,379 --> 00:00:15,878
Depuis le tableau de bord de Signal,

8
00:00:15,878 --> 00:00:17,377
vous pouvez voir les sujets

9
00:00:17,377 --> 00:00:20,956
générant le plus d’engouement en temps réel.

10
00:00:20,956 --> 00:00:22,376
Ces sujets en vogue apparaissent

11
00:00:22,376 --> 00:00:24,254
par ordre de popularité.

12
00:00:24,254 --> 00:00:25,892
Cliquez sur un sujet pour voir

13
00:00:25,892 --> 00:00:27,531
les publications connexes

14
00:00:27,531 --> 00:00:30,664
et obtenir plus de contexte.

15
00:00:30,664 --> 00:00:33,014
Vous pouvez passer à l’écran Catégorie

16
00:00:33,014 --> 00:00:35,365
pour voir les personnalités publiques

17
00:00:35,365 --> 00:00:37,605
les plus en vogue dans le domaine

18
00:00:37,605 --> 00:00:39,846
de la politique, des affaires,

19
00:00:39,846 --> 00:00:44,087
du divertissement, du sport, etc.

20
00:00:44,087 --> 00:00:45,860
Pour découvrir les sujets qui commencent

21
00:00:45,860 --> 00:00:47,634
à créer de l’engouement sur Facebook,

22
00:00:47,634 --> 00:00:50,684
cliquez sur l’onglet Émergent.

23
00:00:50,684 --> 00:00:52,426
Vous pouvez également trouver du contenu Facebook

24
00:00:52,426 --> 00:00:54,169
en utilisant la recherche par mot-clé.

25
00:00:58,089 --> 00:00:59,950
Les résultats de votre recherche vous sont

26
00:00:59,950 --> 00:01:02,120
présentés dans l’ordre chronologique décroissant

27
00:01:02,120 --> 00:01:04,967
(les contenus plus récents sont présentés en premier).

28
00:01:04,967 --> 00:01:06,323
Vous pouvez également trouver

29
00:01:06,323 --> 00:01:07,679
du contenu visuel sur Instagram

30
00:01:07,679 --> 00:01:10,098
en effectuant une recherche par hashtag,

31
00:01:10,098 --> 00:01:12,518
identification de lieu ou nom d’utilisateur.

32
00:01:15,425 --> 00:01:17,874
À mesure que vous trouvez

33
00:01:17,874 --> 00:01:20,323
des publications intéressantes,

34
00:01:20,323 --> 00:01:22,850
vous pouvez les enregistrer dans vos collections

35
00:01:23,880 --> 00:01:25,919
et les intégrer à votre article en ligne

36
00:01:25,919 --> 00:01:28,791
ou votre page web ultérieurement.

37
00:01:28,791 --> 00:01:30,330
Lorsque vous êtes prêt(e) à ajouter

38
00:01:30,330 --> 00:01:31,870
la publication à votre page web,

39
00:01:31,870 --> 00:01:33,940
il vous suffit de copier le code intégré

40
00:01:33,940 --> 00:01:36,150
et de le coller dans votre propre code.

41
00:01:37,180 --> 00:01:38,920
Signal vous permet d’accomplir

42
00:01:38,920 --> 00:01:40,570
bien plus que ce que nous vous avons montré

43
00:01:40,570 --> 00:01:41,951
dans cette brève démonstration.

44
00:01:41,951 --> 00:01:43,500
Pour en savoir plus, commencez à

45
00:01:43,500 --> 00:01:45,049
utiliser Signal dès aujourd’hui.

46
00:01:45,049 --> 00:01:47,867
Envoyez une demande d’accès à partir du lien

47
00:01:47,867 --> 00:01:50,685
dans le dernier chapitre du cours Blueprint.

48
00:01:53,217 --> 00:01:57,918
Ce sera tout pour cette brève démonstration de Signal.

