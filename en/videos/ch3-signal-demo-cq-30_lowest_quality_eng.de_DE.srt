﻿1
00:00:01,479 --> 00:00:05,136
In diesem kurzen Video präsentieren wir die Funktionsweise von Signal,

2
00:00:05,136 --> 00:00:10,019
einem Tool für Journalisten, um Facebook- und Instagram-Content in den Bereichen

3
00:00:10,019 --> 00:00:14,379
Nachrichten, Unterhaltung, Sport und anderen Kategorien abzubilden.

4
00:00:14,379 --> 00:00:17,377
Im Signal-Dashboard kannst du in Echtzeit sehen,

5
00:00:17,377 --> 00:00:20,956
welche Themen die meisten Unterhaltungen verzeichnen.

6
00:00:20,956 --> 00:00:24,254
Die Trendthemen werden dabei nach ihrer Beliebtheit angezeigt.

7
00:00:24,254 --> 00:00:27,531
Klicke auf ein Thema, um ähnliche Beiträge zu sehen –

8
00:00:27,531 --> 00:00:30,664
mit Einstufung und Kontext.

9
00:00:30,664 --> 00:00:35,365
In der Kategorieansicht kannst du sehen, welche Personen des

10
00:00:35,365 --> 00:00:39,846
öffentlichen Lebens besonders viele Posts erstellen. Rubriken sind:

11
00:00:39,846 --> 00:00:44,087
Politik, Wirtschaft, Unterhaltung, Sport usw.

12
00:00:44,087 --> 00:00:47,634
In der Ansicht „Emerging“ findest du Themen, die auf Facebook

13
00:00:47,634 --> 00:00:50,684
eine besondere Rolle spielen.

14
00:00:50,684 --> 00:00:54,169
Du kannst Facebook-Content auch durch eine Stichwortsuche finden.

15
00:00:58,089 --> 00:01:02,222
Die Ergebnisse werden dabei in umgekehrter zeitlicher Reihenfolge angezeigt,

16
00:01:02,222 --> 00:01:04,967
d. h. die neuesten Beiträge zuerst.

17
00:01:04,967 --> 00:01:07,679
Du kannst auch nach visuellen Inhalten auf Instagram suchen.

18
00:01:07,679 --> 00:01:12,518
Nutze dazu Hashtags, Standort-Tags oder Namen von Nutzerkonten.

19
00:01:15,425 --> 00:01:20,323
Wenn du Posts findest, die dich interessieren,

20
00:01:20,323 --> 00:01:23,806
kannst du sie in deinen Sammlungen speichern und später

21
00:01:23,806 --> 00:01:28,791
in deinen Online-Artikel oder deine Webseite einbinden.

22
00:01:28,791 --> 00:01:30,872
Wenn du den Beitrag zu deiner Webseite hinzuzufügen möchtest,

23
00:01:30,872 --> 00:01:37,180
kopiere einfach den Einbettungscode und füge ihn in deinen eigenen Code ein.

24
00:01:37,180 --> 00:01:40,879
Signal bietet deutlich vielfältigere Funktionen als die hier

25
00:01:40,879 --> 00:01:41,951
im Demo-Video gezeigten.

26
00:01:41,951 --> 00:01:45,049
Beginne noch heute mit der Verwendung von Signal.

27
00:01:45,049 --> 00:01:53,217
Fordere deinen Zugriff an, indem du im letzten Kapitel des Blueprint-Kurses auf den entsprechenden Link klickst.

28
00:01:53,217 --> 00:01:57,918
Damit endet unsere kurze Präsentation zu Signal.

