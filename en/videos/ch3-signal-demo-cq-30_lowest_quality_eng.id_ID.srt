﻿1
00:00:01,479 --> 00:00:05,136
Dalam video pendek ini, akan disoroti kapabilitas Signal,

2
00:00:05,136 --> 00:00:10,019
alat yang dibuat untuk para jurnalis untuk menampilkan konten Facebook dan Instagram

3
00:00:10,019 --> 00:00:14,379
di berbagai konten berita, hiburan, olahraga, serta kategori konten lainnya.

4
00:00:14,379 --> 00:00:17,377
Dari dasbor Signal, Anda dapat melihat topik mana

5
00:00:17,377 --> 00:00:20,262
yang akan menghasilkan paling banyak percakapan secara realtime.

6
00:00:20,956 --> 00:00:23,797
Topik yang sedang tren akan muncul berdasar urutan popularitasnya.

7
00:00:24,254 --> 00:00:27,531
Klik masing-masing topik untuk melihat kiriman terkait

8
00:00:27,531 --> 00:00:30,315
untuk menilai warna dan konteksnya.

9
00:00:30,664 --> 00:00:35,365
Anda bisa beralih ke tampilan kategori untuk melihat siapa tokoh publik

10
00:00:35,365 --> 00:00:39,846
yang banyak dibicarakan berdasarkan domain politik, bisnis,

11
00:00:39,846 --> 00:00:44,087
hiburan, olahraga, dan domain lainnya.

12
00:00:44,087 --> 00:00:47,634
Untuk mempelajari topik mana yang mulai menghangat di Facebook,

13
00:00:47,634 --> 00:00:49,862
klik tampilan yang sedang berkembang.

14
00:00:50,684 --> 00:00:54,169
Anda dapat menampilkan konten Facebook dengan menggunakan pencarian kata kunci.

15
00:00:58,089 --> 00:01:02,222
Hasil yang Anda peroleh ditampilkan dengan urutan kronologis terbalik

16
00:01:02,222 --> 00:01:04,222
dengan yang paling baru ditampilkan di paling atas.

17
00:01:04,967 --> 00:01:07,679
Anda dapat menemukan konten visual di Instagram

18
00:01:07,679 --> 00:01:12,518
dengan mencari tagar, label lokasi, atau nama akun pengguna.

19
00:01:15,425 --> 00:01:20,323
Selagi melihat-lihat kiriman yang menarik perhatian,

20
00:01:20,323 --> 00:01:22,924
Anda dapat menyimpannya ke koleksi

21
00:01:23,806 --> 00:01:28,032
untuk Anda gunakan nanti guna mengintegrasikan ke artikel online Anda atau halaman web.

22
00:01:28,791 --> 00:01:30,872
Setelah siap menambahkan kiriman ke halaman web,

23
00:01:30,872 --> 00:01:36,216
cukup salin kode yang disematkan dan tempelkan ke kode Anda.

24
00:01:37,180 --> 00:01:40,879
Masih banyak yang dapat Anda lakukan dengan Signal dari yang bisa kami tampilkan

25
00:01:40,879 --> 00:01:41,951
dalam demo pendek ini.

26
00:01:41,951 --> 00:01:45,049
Untuk mempelajari selengkapnya, mulai gunakan Signal sekarang juga.

27
00:01:45,049 --> 00:01:50,685
Anda bisa meminta akses dengan menggunakan tautan di bab terakhir kursus blueprint.

28
00:01:53,217 --> 00:01:57,918
Di sini Anda dapat menemukan demo singkat Signal.


