﻿1
00:00:00,000 --> 00:00:01,684
‫[تسجيل الدخول إلى Signal باستخدام فيسبوك]

2
00:00:01,684 --> 00:00:05,612
‫في هذا الفيديو القصير،
‫سوف نسلط الضوء على إمكانات Signal

3
00:00:05,612 --> 00:00:10,453
‫وهو عبارة عن أداة يستخلص من خلالها الصحفيون
‫المحتوى من فيسبوك وInstagram

4
00:00:10,453 --> 00:00:14,950
‫في مجال الأخبار والترفيه والرياضة
‫وغيرها من فئات المحتوى

5
00:00:14,950 --> 00:00:16,185
‫من لوحة معلومات Signal

6
00:00:16,185 --> 00:00:20,888
‫يمكنك معرفة الموضوعات التي تحقق
‫أكبر قدر من النقاش في الوقت الفعلي

7
00:00:20,888 --> 00:00:24,519
‫تظهر هذه الموضوعات الرائجة
‫بترتيب شهرتها

8
00:00:24,519 --> 00:00:30,817
‫انقر على أي موضوع
‫لعرض المنشورات المرتبطة به لمزيد من التفاصيل والسياق

9
00:00:30,817 --> 00:00:34,246
‫يمكنك التبديل إلى طريقة العرض Category لمعرفة أكثر

10
00:00:34,246 --> 00:00:37,068
‫الشخصيات العامة التي يتحدث عنها الأشخاص

11
00:00:37,068 --> 00:00:41,988
‫في مجال السياسة والأعمال والترفيه

12
00:00:41,988 --> 00:00:44,801
‫والرياضة وغيرها من المجالات

13
00:00:44,801 --> 00:00:48,248
‫لتتعرف على الموضوعات التي بدأت
‫في اكتساب شهرة على فيسبوك

14
00:00:48,248 --> 00:00:50,867
‫انقر على طريقة العرض Emerging

15
00:00:50,867 --> 00:00:54,516
‫يمكنك أيضًا استخلاص المحتوى من فيسبوك
‫بالبحث عن الكلمات الأساسية

16
00:00:58,080 --> 00:01:02,584
‫يتم عرض النتائج التي تظهر لك
‫بترتيب زمني معكوس

17
00:01:02,584 --> 00:01:05,205
‫حيث تظهر أحدث النتائج في الأعلى

18
00:01:05,205 --> 00:01:08,061
‫يمكنك أيضًا العثور على محتوى بعناصر بصرية في Instagram

19
00:01:08,061 --> 00:01:12,797
‫بالبحث عن علامات الهاشتاج أو إشارات الموقع
‫أو أسماء حسابات المستخدمين

20
00:01:15,565 --> 00:01:20,863
‫عندما تعثر على منشورات تهمك

21
00:01:20,863 --> 00:01:24,239
‫يمكنك حفظها إلى مقتطفاتك

22
00:01:24,239 --> 00:01:28,756
‫لاستخدامها لاحقًا وتضمينها
‫في مقالاتك على الإنترنت أو صفحتك على الويب

23
00:01:28,756 --> 00:01:31,336
‫عندما تكون مستعدًا لإضافة منشور إلى صفحتك على الويب

24
00:01:31,336 --> 00:01:37,176
‫فقط اسحب رمز التضمين
‫وانسخه لتضعه ضمن الرمز الخاص بك

25
00:01:37,176 --> 00:01:39,891
‫وهناك الكثير من الأمور التي يمكنك تنفيذها باستخدام Signal

26
00:01:39,891 --> 00:01:42,212
‫بخلاف ما عرضناه في هذا العرض التوضيحي القصير

27
00:01:42,212 --> 00:01:45,390
‫للتعرف على المزيد، بادر إلى استخدام Signal اليوم

28
00:01:45,390 --> 00:01:48,445
‫يمكنك طلب إمكانية الاستخدام بالنقر على الرابط

29
00:01:48,445 --> 00:01:51,035
‫الموجود بآخر فصل من فصول دورة Blueprint التدريبية

30
00:01:53,411 --> 00:01:58,152
‫كان ذلك تلخيصًا لهذا العرض التوضيحي القصير عن Signal

