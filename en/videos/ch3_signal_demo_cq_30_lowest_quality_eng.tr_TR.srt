﻿1
00:00:00,000 --> 00:00:01,684
[Facebook ile Signal'a Giriş Yapma]

2
00:00:01,684 --> 00:00:05,612
Bu kısa videoda, gazetecilerin
haber, eğlence, spor ve diğer içerik

3
00:00:05,612 --> 00:00:10,453
kategorilerinde Facebook ve Instagram
içeriklerini keşfetmeleri için

4
00:00:10,453 --> 00:00:14,950
oluşturulmuş bir araç olan
Signal'ın özelliklerine değineceğiz.

5
00:00:14,950 --> 00:00:16,185
Signal panosunda,

6
00:00:16,185 --> 00:00:20,888
gerçek zamanlı olarak hangi konularda
en fazla konuşma yapıldığını görebilirsiniz.

7
00:00:20,888 --> 00:00:24,519
Gündemdeki bu konular,
popülerlik sırasına göre gösterilir.

8
00:00:24,519 --> 00:00:30,817
Daha fazla renk ve bağlam için
istediğiniz konuya tıklayarak alakalı gönderileri görebilirsiniz.

9
00:00:30,817 --> 00:00:34,246
Siyaset, ticaret, eğlence, spor ve diğer alanlarda

10
00:00:34,246 --> 00:00:37,068
insanların en fazla hangi tanınmış kişiler hakkında

11
00:00:37,068 --> 00:00:41,988
konuştuğunu görmek için Kategori görünümüne

12
00:00:41,988 --> 00:00:44,801
geçiş yapabilirsiniz.

13
00:00:44,801 --> 00:00:48,248
Facebook'ta hangi konuların popülerlik
kazanmaya başladığını öğrenmek için

14
00:00:48,248 --> 00:00:50,867
Yükseliştekiler görünümüne tıklayın.

15
00:00:50,867 --> 00:00:54,516
Anahtar sözcük aramasını kullanarak da
Facebook içeriklerini keşfedebilirsiniz.

16
00:00:58,080 --> 00:01:02,584
Gördüğünüz sonuçlar, ters
bir kronolojik sırayla sunulur ve

17
00:01:02,584 --> 00:01:05,205
en yeni sonuçlar en üstte gösterilir.

18
00:01:05,205 --> 00:01:08,061
Konu etiketleri, konum etiketleri veya

19
00:01:08,061 --> 00:01:12,797
kullanıcı hesabı adlarını aratarak da
Instagram'daki görsel içerikleri bulabilirsiniz.

20
00:01:15,565 --> 00:01:20,863
İlginizi çeken gönderilerle karşılaştığınızda,

21
00:01:20,863 --> 00:01:24,239
bunları koleksiyonlarınıza kaydedebilir ve

22
00:01:24,239 --> 00:01:28,756
daha sonra internet makalenize veya
sayfanıza entegre edebilirsiniz.

23
00:01:28,756 --> 00:01:31,336
Gönderiyi sayfanıza eklemeye hazır olduğunuzda,

24
00:01:31,336 --> 00:01:37,176
gömme kodunu alın ve kendi
kodunuzun içine yapıştırın.

25
00:01:37,176 --> 00:01:39,891
Signal ile, bu hızlı demoda gösterdiklerimizden

26
00:01:39,891 --> 00:01:42,212
çok daha fazlasını yapabilirsiniz.

27
00:01:42,212 --> 00:01:45,390
Daha fazla bilgi almak için, bugün Signal'ı kullanmaya başlayın.

28
00:01:45,390 --> 00:01:48,445
Blueprint kursunun son bölümündeki

29
00:01:48,445 --> 00:01:51,035
bağlantıyı kullanarak erişim isteyin.

30
00:01:53,411 --> 00:01:58,152
Bu kısa Signal demosunun sonuna geldik.

