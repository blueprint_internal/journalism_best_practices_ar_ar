﻿1
00:00:00,760 --> 00:00:04,987
Esta é uma introdução à ferramenta Intelligence da CrowdTangle.

2
00:00:04,987 --> 00:00:10,814
A ferramenta Intelligence fornece estatísticas periódicas a nível de conta com gráficos e tabelas.

3
00:00:10,814 --> 00:00:15,174
Funciona em todas as plataformas cobertas pela CrowdTangle,

4
00:00:15,174 --> 00:00:19,565
incluindo Facebook, Instagram, Twitter e Reddit.

5
00:00:19,565 --> 00:00:23,301
Essa ferramenta ajudará os editores a entenderem as tendências gerais

6
00:00:23,301 --> 00:00:26,548
e a analisarem com ainda mais facilidade qual conteúdo está funcionando

7
00:00:26,548 --> 00:00:28,737
e qual conteúdo não está funcionando.

8
00:00:28,737 --> 00:00:34,901
Para usar esta ferramenta, basta conectar sua própria conta na barra de pesquisa superior.

9
00:00:34,901 --> 00:00:42,456
Você pode classificar por tipo de publicação, fotos, links, vídeos e tipo de interação,

10
00:00:42,456 --> 00:00:45,602
curtidas, comentários e compartilhamentos.

11
00:00:45,602 --> 00:00:51,831
É possível conectar até cinco páginas diferentes na barra de pesquisa na parte superior

12
00:00:51,831 --> 00:00:54,956
para realizar uma análise mais competitiva.

13
00:00:54,956 --> 00:01:00,165
E o mais importante, você pode exportar este relatório como PDF

14
00:01:00,165 --> 00:01:03,221
ou baixá-lo como um arquivo CSV.

15
00:01:03,221 --> 00:01:07,858
Você também pode transformar este relatório em um link público

16
00:01:07,858 --> 00:01:12,487
que pode ser compartilhado com sua equipe, mesmo que ela não tenha a CrowdTangle.

17
00:01:13,861 --> 00:01:18,201
Em caso de dúvidas, não hesite em entrar em contato com a equipe da CrowdTangle

18
00:01:18,201 --> 00:01:21,219
usando o botão de bate-papo na parte inferior direita.

19
00:01:21,219 --> 00:01:24,086
Divirta-se com os recursos da CrowdTangle.

