﻿1
00:00:00,760 --> 00:00:04,987
Ini akan menjadi pengenalan alat Intelligence dari CrowdTangle.

2
00:00:04,987 --> 00:00:10,814
Dengan Intelligence, Anda akan mendapatkan statistik level akun dengan grafik dan bagan.

3
00:00:10,814 --> 00:00:15,174
Intelligence dapat digunakan di berbagai platform yang dicakup oleh CrowdTangle

4
00:00:15,174 --> 00:00:19,433
termasuk Facebook, Instagram, Twitter, dan Reddit.

5
00:00:19,565 --> 00:00:23,301
Alat ini membantu penerbit memahami seluruh tren

6
00:00:23,301 --> 00:00:26,548
dan dapat dengan mudah menganalisis konten apa yang berfungsi

7
00:00:26,548 --> 00:00:28,737
dan konten apa yang tidak berfungsi

8
00:00:28,737 --> 00:00:33,646
Untuk menggunakan Intelligence, cukup masukkan akun Anda di bilah pencarian atas.

9
00:00:34,901 --> 00:00:42,456
Anda dapat menyortir berdasarkan jenis kiriman, foto, tautan, video, dan jenis interaksi,

10
00:00:42,456 --> 00:00:44,941
suka, komentar, dan berbagi.

11
00:00:45,602 --> 00:00:51,831
Anda bisa memasukkan hingga lima halaman berbeda di bilah pencarian pada bagian atas

12
00:00:51,831 --> 00:00:54,446
untuk melakukan analisis kompetitif

13
00:00:54,956 --> 00:01:00,165
Dan yang terpenting, Anda bisa mengekspor laporan ini sebagai PDF

14
00:01:00,165 --> 00:01:03,221
atau mengunduhnya sebagai file CSV.

15
00:01:03,221 --> 00:01:07,858
Anda juga bisa mengubah laporan ini menjadi tautan publik

16
00:01:07,858 --> 00:01:12,487
yang bisa Anda bagikan kepada tim, meskipun mereka tidak memiliki CrowdTangle.

17
00:01:13,861 --> 00:01:18,201
Jika ada pertanyaan, segera hubungi tim CrowdTangle

18
00:01:18,201 --> 00:01:21,219
dengan tombol obrolan di kanan bawah.

19
00:01:21,219 --> 00:01:24,086
Selamat menikmati keseruan bersama CrowdTangle.


